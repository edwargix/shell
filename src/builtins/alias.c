#include <stdio.h>
#include <string.h>

#include "alias.h"
#include "error.h"
#include "interpreter.h"
#include "shell_builtins.h"

static int alias_builtin(struct interpreter_state *state,
			 const char *const *argv, int input_fd, int output_fd,
			 int error_fd)
{
	FILE *output_fp = fdopen(output_fd, "w");
	const char *const *p;
	char *saveptr, *name, *replacement;

	CHECK(output_fp);
	CHECK(argv && argv[0]);

	if (!argv[1]) {
		print_aliases(state->aliases, output_fp);
		fflush(output_fp);
		return 0;
	}

	for (p = argv+1; *p != NULL; ++p) {
		name = strtok_r(strdup(*p), "=", &saveptr);
		replacement = strtok_r(NULL, "=", &saveptr);
		alias_set(state->aliases, name, replacement);
	}

	fflush(output_fp);
	return 0;
}
DEFINE_BUILTIN_COMMAND("alias", alias_builtin);
