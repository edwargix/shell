#include "alias.h"
#include "error.h"
#include "interpreter.h"
#include "shell_builtins.h"

static int unalias_builtin(struct interpreter_state *state,
			   const char *const *argv, int input_fd, int output_fd,
			   int error_fd)
{
	const char *const *p;

	CHECK(argv && argv[0]);

	for (p = argv+1; *p != NULL; ++p)
		alias_unset(state->aliases, *p);

	return 0;
}
DEFINE_BUILTIN_COMMAND("unalias", unalias_builtin);
