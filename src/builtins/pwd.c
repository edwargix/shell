#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>

#include "error.h"
#include "interpreter.h"
#include "shell_builtins.h"
#include "unit.h"

static int pwd_builtin(struct interpreter_state *state,
		       const char *const *argv, int input_fd, int output_fd,
		       int error_fd)
{
	FILE *output_fp = fdopen(output_fd, "w");
	FILE *error_fp = fdopen(error_fd, "w");

	CHECK(output_fp);
	CHECK(argv && argv[0]);

	if (argv[1]) {
		fprintf(error_fp, "%s: too many arguments\n", argv[0]);
		return 1;
	}

	fprintf(output_fp, "%s\n", get_current_dir_name());
	fflush(output_fp);
	fflush(error_fp);

	return 0;
}
DEFINE_BUILTIN_COMMAND("pwd", pwd_builtin);
