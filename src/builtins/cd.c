#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "error.h"
#include "interpreter.h"
#include "shell_builtins.h"
#include "unit.h"

static int cd_builtin(struct interpreter_state *state,
		      const char *const *argv, int input_fd, int output_fd,
		      int error_fd)
{
	FILE *error_fp = fdopen(error_fd, "w");

	CHECK(error_fp);
	CHECK(argv && argv[0]);

	char *dir = (char *) argv[1];

	if (!dir)
		asprintf(&dir, "%s", getenv("HOME"));

	if (chdir(dir) == -1) {
		fprintf(error_fp, "%s: %s\n", argv[0], strerror(errno));
		fflush(error_fp);
		return -1;
	}

	return 0;
}
DEFINE_BUILTIN_COMMAND("cd", cd_builtin);
