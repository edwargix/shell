#include <stdio.h>

#include "error.h"
#include "interpreter.h"
#include "shell_builtins.h"
#include "unit.h"

static int history_builtin(struct interpreter_state *state,
			   const char *const *argv, int input_fd, int output_fd,
			   int error_fd)
{
	HIST_ENTRY **p;
	int i;
	FILE *output_fp = fdopen(output_fd, "w");

	CHECK(output_fp);
	CHECK(argv && argv[0]);

	for (i = 1, p = state->history_list; *p != NULL; ++p) {
		fprintf(output_fp, "\t%d\t%s\n", i++, (*p)->line);
	}

	fflush(output_fp);

	return 0;
}
DEFINE_BUILTIN_COMMAND("history", history_builtin);
