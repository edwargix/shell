#include <stdio.h>

#include "error.h"
#include "interpreter.h"
#include "shell_builtins.h"

static int echo_builtin(struct interpreter_state *state,
			const char *const *argv, int input_fd, int output_fd,
			int error_fd)
{
	const char *const *p;

	FILE *output_fp = fdopen(output_fd, "w");

	CHECK(output_fp);
	CHECK(argv && argv[0]);

	if (argv[1]) {
		fprintf(output_fp, "%s", argv[1]);
		for (p = argv + 2; *p != NULL; ++p)
			fprintf(output_fp, " %s", *p);
	}
	fputc('\n', output_fp);
	fflush(output_fp);

	return 0;
}
DEFINE_BUILTIN_COMMAND("echo", echo_builtin);
