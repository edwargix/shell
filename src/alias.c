#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "error.h"

struct alias_entry {
	const char *name;
	const char *replacement;
	struct alias_entry *next;
};

struct alias_table {
	struct alias_entry *entries;
};

struct alias_table *alias_table_new(void)
{
	/* does zeroing for us */
	return checked_calloc(sizeof(struct alias_table), 1);
}

void alias_table_free(struct alias_table *table)
{
	struct alias_entry *p, *tmp;

	CHECK(table);

	for (p = table->entries; p != NULL; ) {
		tmp = p;
		p = p->next;
		free(tmp);
	}
}

void alias_set(struct alias_table *table, const char *name,
	       const char *replacement)
{
	struct alias_entry **pp;

	CHECK(name);
	CHECK(replacement);

	for (pp = &table->entries; *pp != NULL; pp = &(*pp)->next)
		if (strcmp((*pp)->name, name) == 0) {
			(*pp)->replacement = replacement;
			return;
		}

	*pp = checked_malloc(sizeof(struct alias_entry), 1);
	(*pp)->name = name;
	(*pp)->replacement = replacement;
	(*pp)->next = NULL;
}

void alias_unset(struct alias_table *table, const char *name)
{
	struct alias_entry **pp;

	CHECK(table);
	CHECK(name);

	for (pp = &table->entries; *pp != NULL; pp = &(*pp)->next)
		if (strcmp((*pp)->name, name) == 0) {
			struct alias_entry *tmp = *pp;
			*pp = (*pp)->next;
			free(tmp);
			return;
		}
}

const char *alias_get(struct alias_table *table, const char *name)
{
	struct alias_entry *ae;

	CHECK(table);
	CHECK(name);

	for (ae = table->entries; ae != NULL; ae = ae->next)
		if (strcmp(name, ae->name) == 0)
			return ae->replacement;
	return NULL;
}

void print_aliases(struct alias_table *table, FILE *f)
{
	struct alias_entry *ae;

	for (ae = table->entries; ae != NULL; ae = ae->next)
		fprintf(f, "%s=%s\n", ae->name, ae->replacement);
}
