#include <stdlib.h>

#include "alias.h"
#include "arena.h"
#include "ast.h"
#include "error.h"
#include "string_builder.h"

char *parts_to_string(struct arena *arena,
		      struct ast_argument_part_list *parts,
		      struct alias_table *vars)
{
	CHECK(arena);
	CHECK(parts);
	CHECK(vars);

	struct ast_argument_part_list *p;
	struct ast_string *ast_str;
	const char *str, *name;

	struct string_builder *builder = string_builder_new(arena);

	for (p = parts; p != NULL; p = p->rest)
		if ((ast_str = p->first->string))
			string_builder_sized_append(
				builder, ast_str->data, ast_str->size);
		else if ((ast_str = p->first->parameter)) {
			name = arena_strndup(arena, ast_str->data, ast_str->size);
			if ((str = getenv(name)) || (str = alias_get(vars, name)))
				string_builder_append(builder, str);
		} else
			TODO("unhandled part type");

	return string_builder_finalize(builder);
}
