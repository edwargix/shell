#include <string.h>

#include "alias.h"
#include "arena.h"
#include "ast.h"
#include "error.h"
#include "parts.h"
#include "string_builder.h"

struct argv_builder_entry {
	char *str;
	struct argv_builder_entry *rest;
};

struct argv_builder {
	struct argv_builder_entry *entries;
	struct argv_builder_entry *last;
	size_t total_size;
	struct arena *arena;
};

struct argv_builder *argv_builder_new(struct arena *arena)
{
	struct argv_builder *ab =
		arena_malloc(arena, sizeof(struct argv_builder), 1);
	ab->entries = NULL;
	ab->last = NULL;
	ab->total_size = 0;
	ab->arena = arena;
	return ab;
}

void argv_builder_append(struct argv_builder *ab, char *str)
{
	struct argv_builder_entry *entry;

	CHECK(str);

	entry = arena_malloc(ab->arena, sizeof(struct argv_builder_entry), 1);
	entry->str = str;
	entry->rest = NULL;

	ab->total_size += 1;
	if (ab->entries)
		ab->last->rest = entry;
	else
		ab->entries = entry;
	ab->last = entry;
}

char **argv_builder_finalize(struct argv_builder *ab)
{
	size_t size = ab->total_size + 1;
	char **buf = arena_malloc(ab->arena, sizeof(char *), size);
	char **p = buf;

	struct argv_builder_entry *entry = ab->entries;

	while (entry) {
		*p = entry->str;
		p += 1;
		entry = entry->rest;
	}
	*p = NULL;

	return buf;
}

size_t argv_builder_length(struct argv_builder *ab)
{
	return ab->total_size;
}

char **arglist_to_argv(struct arena *arena,
		       struct ast_argument_list *arglist,
		       struct alias_table *vars)
{
	CHECK(arena);
	CHECK(arglist);
	CHECK(vars);

	struct ast_argument_list *p;
	struct argv_builder *argv_builder = argv_builder_new(arena);

	for (p = arglist; p != NULL; p = p->rest)
		argv_builder_append(
			argv_builder,
			parts_to_string(arena, p->first->parts, vars));

	return argv_builder_finalize(argv_builder);
}
