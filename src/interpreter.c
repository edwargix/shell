#include <stdlib.h>

#include "alias.h"
#include "error.h"
#include "interpreter.h"

struct interpreter_state *interpreter_new()
{
	struct interpreter_state *interp =
		checked_calloc(sizeof(struct interpreter_state), 1);

	interp->aliases = alias_table_new();
	interp->vars = alias_table_new();

	return interp;
}

void interpreter_free(struct interpreter_state *interp)
{
	if (interp->aliases)
		alias_table_free(interp->aliases);
	if (interp->vars)
		alias_table_free(interp->vars);
	free(interp);
}
