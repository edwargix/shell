#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include "alias.h"
#include "arena.h"
#include "argv_builder.h"
#include "error.h"
#include "interpreter.h"
#include "parser.h"
#include "parts.h"
#include "shell_builtins.h"
#include "string_builder.h"


static struct arena arena = { NULL };
static char *prompt = NULL;
static char *hostname = NULL;


void update_prompt(int ret)
{
	if (hostname == NULL) {
		size_t len = 1024;
		hostname = arena_malloc(&arena, sizeof(char) * len, 1);
		if (gethostname(hostname, len) == -1) {
			perror("gethostname");
			strcpy(hostname, "");
		}
	}

	if (asprintf(&prompt, "%s@%s:%s %s $ ",
		     getlogin(),
		     hostname,
		     get_current_dir_name(),
		     ret == 0 ? ":)" : ":(") == -1)
		perror("asprintf");
}


int exec_stmnt(struct ast_statement *stmnt, struct interpreter_state *state)
{
	if (!stmnt)
		return 0;

	int ret = 0;
	pid_t pid;
	int wstatus;

	struct ast_argument_list *arglist;
	struct ast_assignment_list *aal;
	struct ast_command *cmd;
	struct ast_pipeline *pipeline;
	struct builtin_command *bcmd;

	int read_input_fd = STDIN_FILENO;
	int read_output_fd = -1;
	int write_output_fd = STDOUT_FILENO;

	const char *str;

	const char *name;
	const char *value;

	for (pipeline = stmnt->pipeline;
	     pipeline != NULL && (cmd = pipeline->first);
	     pipeline = pipeline->rest) {
		arglist = cmd->arglist;

		/* need to read from last cmd if previous cmd in pipeline */
		if (write_output_fd != STDOUT_FILENO)
			read_input_fd = read_output_fd;
		else if (cmd->input_file) {
			read_input_fd = open(
				parts_to_string(&arena,
						cmd->input_file->parts,
						state->vars),
				O_RDONLY);
			if (read_input_fd == -1) {
				perror("shell");
				return -1;
			}
		} else
			read_input_fd = STDIN_FILENO;

		/* need to pipe if there's a following cmd in pipeline */
		if (pipeline->rest != NULL) {
			int fd[2];
			if (pipe(fd) == -1) {
				perror("pipe");
				return -1;
			}
			read_output_fd = fd[0];
			write_output_fd = fd[1];
		} else if (cmd->output_file) {
			read_output_fd = -1;
			write_output_fd = open(
				parts_to_string(&arena,
						cmd->output_file->parts,
						state->vars),
				O_WRONLY | O_CREAT | O_TRUNC,
				0666);
			if (write_output_fd == -1) {
				perror("shell");
				return -1;
			}
		} else if (cmd->append_file) {
			read_output_fd = -1;
			write_output_fd = open(
				parts_to_string(&arena,
						cmd->append_file->parts,
						state->vars),
				O_WRONLY | O_APPEND | O_CREAT,
				0666);
			if (write_output_fd == -1) {
				perror("shell");
				return -1;
			}
		} else {
			read_output_fd = -1;
			write_output_fd = STDOUT_FILENO;
		}

		if (arglist) {
			char **argv = arglist_to_argv(&arena, arglist, state->vars);

			/* swap alias with replacement */
			if ((str = alias_get(state->aliases, argv[0])))
				argv[0] = arena_strdup(&arena, str);

			/* check for builtin command, otherwise use exec */
			if ((bcmd = builtin_command_get(argv[0]))) {
				ret = bcmd->function(state,
						     (const char *const *) argv,
						     read_input_fd,
						     write_output_fd,
						     STDERR_FILENO);
			} else {
				if ((pid = fork()) == -1) {
					perror("fork");
					return -1;
				} else if (pid == 0) {
					/* don't need read end of child
					 * output */
					if (read_output_fd != -1)
						close(read_output_fd);
					/* make stdin point to input read fd */
					if (dup2(read_input_fd, STDIN_FILENO) == -1) {
						perror("dup2");
						exit(-1);
					}
					/* make stdout point to output write
					 * fd */
					if (dup2(write_output_fd, STDOUT_FILENO) == -1) {
						perror("dup2");
						exit(-1);
					}
					execvpe(argv[0], argv, environ);
					perror(argv[0]);
					exit(-1);
				} else {
					if (waitpid(pid, &wstatus, 0) == -1) {
						perror("waitpid");
						return -1;
					}
					if (WIFEXITED(wstatus))
						ret = WEXITSTATUS(wstatus);
					else
						ret = -1;
				}
			}
			/* no longer need write end of child output */
			if (write_output_fd != STDOUT_FILENO)
				if (close(write_output_fd) == -1) {
					perror("close");
					return -1;
				}
		} else {
			ret = 0;
		}

		update_prompt(ret);

		for (aal = cmd->assignments;
		     aal != NULL;
		     aal = aal->rest) {
			name = arena_strndup(&arena,
					     aal->first->name->data,
					     aal->first->name->size);
			value = parts_to_string(&arena,
						aal->first->value->parts,
						state->vars);
			if (getenv(name)) {
				if (setenv(name, value, 1) == -1) {
					perror("setenv");
					return -1;
				}
			} else
				alias_set(state->vars, name, value);
		}
	}

	/* final command in pipeline is done reading */
	if (read_input_fd != STDIN_FILENO)
		if (close(read_input_fd) == -1) {
			perror("close");
			return -1;
		}

	return ret;
}


int main(int argc, char *argv[])
{
	char *raw_line;
	char *line;

	struct ast_statement_list *asl;
	struct interpreter_state *state = interpreter_new();

	update_prompt(0);
	using_history();

	while ((raw_line = readline(prompt)) != NULL) {
		switch (history_expand(raw_line, &line)) {
		case 1:
			if (puts(line) == EOF) {
				perror("puts");
				return -1;
			}
		case 0:
			if (!line[0])
				break;
			add_history(line);
			state->history_list = history_list();
			for (asl = parse_input(line);
			     asl != NULL;
			     asl = asl->rest) {
				update_prompt(exec_stmnt(asl->first, state));
			}
			break;
		case 2:
			if (*line)
				puts(line);
			break;
		case -1:
			fprintf(stderr, "%s: %s", argv[0], line);
			free(raw_line);
			return 1;
		}
		free(raw_line);
	}

	putchar('\n');
	free(prompt);

	interpreter_free(state);
	arena_free(&arena);

	return 0;
}
