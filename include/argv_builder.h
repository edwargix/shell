#ifndef _ARGV_BUILDER_H_
#define _ARGV_BUILDER_H_

#include <stddef.h>

#include "alias.h"
#include "ast.h"

struct arena;
struct string_builder;

struct argv_builder *argv_builder_new(struct arena *arena);
void argv_builder_append(struct argv_builder *ab, const char *str);
char **argv_builder_finalize(struct argv_builder *ab);
size_t argv_builder_length(struct argv_builder *ab);
char **arglist_to_argv(struct arena *arena,
		       struct ast_argument_list *arglist,
		       struct alias_table *vars);

#endif /* _ARGV_BUILDER_H_ */
