#ifndef PARTS_H
#define PARTS_H

#include "alias.h"
#include "ast.h"

char *parts_to_string(struct arena *arena,
		      struct ast_argument_part_list *parts,
		      struct alias_table *vars);

#endif /* PARTS_H */

