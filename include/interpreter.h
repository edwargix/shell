#ifndef _INTERPRETER_H
#define _INTERPRETER_H

#include <stdbool.h>
#include <stdio.h>
#include <readline/history.h>

#include "ast.h"

struct interpreter_state {
	struct alias_table *aliases;
	HIST_ENTRY **history_list;
	struct alias_table *vars;
};

struct interpreter_state *interpreter_new();
void interpreter_free(struct interpreter_state *interp);

#endif /* _INTERPRETER_H */
